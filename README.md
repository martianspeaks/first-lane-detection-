# First Lane Detection 

This is my first try at lane detection using OpenCV. It is implemented by using some basic image processing techniques, including
->Dilation
->HoughLine detection
->Masks
->Canny Edge Detection
